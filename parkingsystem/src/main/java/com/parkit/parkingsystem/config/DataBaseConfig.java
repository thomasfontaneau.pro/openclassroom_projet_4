package com.parkit.parkingsystem.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import java.sql.*;

public class DataBaseConfig {
    private static final Logger logger = LogManager.getLogger("DataBaseConfig");

    InputStream input = DataBaseConfig.class.getClassLoader().getResourceAsStream("configDB.properties");
    Properties prop = new Properties();

    public Connection getConnection() throws ClassNotFoundException, SQLException, IOException {
        prop.load(input);
        logger.info("Create DB connection");
        Class.forName("com.mysql.cj.jdbc.Driver");
        return DriverManager.getConnection(
                prop.getProperty("db.prod.url"),prop.getProperty("db.prod.user"),prop.getProperty("db.prod.password"));
    }

    public void closeConnection(Connection con){
        if(con!=null){
            try {
                con.close();
                logger.info("Closing DB connection");
            } catch (SQLException e) {
                logger.error("Error while closing connection",e);
            }
        }
    }

    public void closePreparedStatement(PreparedStatement ps) {
        if(ps!=null){
            try {
                ps.close();
                logger.info("Closing Prepared Statement");
            } catch (SQLException e) {
                logger.error("Error while closing prepared statement",e);
            }
        }
    }

    public void closeResultSet(ResultSet rs) {
        if(rs!=null){
            try {
                rs.close();
                logger.info("Closing Result Set");
            } catch (SQLException e) {
                logger.error("Error while closing result set",e);
            }
        }
    }
}
