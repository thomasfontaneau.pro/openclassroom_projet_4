package com.parkit.parkingsystem.service;

import com.parkit.parkingsystem.constants.Fare;
import com.parkit.parkingsystem.dao.ParkingSpotDAO;
import com.parkit.parkingsystem.dao.TicketDAO;
import com.parkit.parkingsystem.model.Ticket;

import java.util.Map;

public class FareCalculatorService {
    /**
     * Calculate the price to pay for the ticket.
     * @param ticket parking information
     * @param regularUser is a user regular
     */
    public void calculateFare(Ticket ticket, boolean regularUser){
        if( (ticket.getOutTime() == null) || (ticket.getOutTime().before(ticket.getInTime())) ){
            throw new IllegalArgumentException("Out time provided is incorrect:"+ticket.getOutTime().toString());
        }

        int inHour = (int) ticket.getInTime().getTime();
        int outHour = (int) ticket.getOutTime().getTime();

        double duration = (double)(outHour - inHour) / 3600000;
        duration = freeUnder30min(duration);
        if (regularUser){
            duration = duration*0.95;
        }
        switch (ticket.getParkingSpot().getParkingType()){
            case CAR: {
                ticket.setPrice(roundedPrice(duration * Fare.CAR_RATE_PER_HOUR));
                break;
            }
            case BIKE: {
                ticket.setPrice(roundedPrice(duration * Fare.BIKE_RATE_PER_HOUR));
                break;
            }
            default: throw new IllegalArgumentException("Unkown Parking Type");
        }
    }

    /**
     * Is the parking time less than 30 min
     * @param duration parking time
     * @return the new duration
     */
    private double freeUnder30min(double duration) {
        if(duration <= 0.50) {
            duration = 0.00;
        }
        return duration;
    }

    /**
     * Rearrange the price to two decimal places
     * @param price price before rounding
     * @return price after rounding
     */
    public double roundedPrice(double price){
        return (double) Math.round((price)*100)/100;
    }
}