package com.parkit.parkingsystem.dao;

import com.parkit.parkingsystem.config.DataBaseConfig;
import com.parkit.parkingsystem.constants.DBConstants;
import com.parkit.parkingsystem.constants.ParkingType;
import com.parkit.parkingsystem.integration.config.DataBaseTestConfig;
import com.parkit.parkingsystem.integration.service.DataBasePrepareService;
import com.parkit.parkingsystem.model.ParkingSpot;
import com.parkit.parkingsystem.model.Ticket;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Date;

public class TicketDaoTest {

    private static final String REGISTERED_PLATE = "QSDFG";
    private static final String NEW_VEHICLE_PLATE = "NEW";

    private static DataBasePrepareService dataBasePrepareService;

    private static DataBaseConfig databaseTestConfig = new DataBaseTestConfig();

    private static TicketDAO ticketDAO;

    static Connection con;

    @BeforeAll
    static void setUp_testEnvironment() throws Exception {

        // Set Up DataBase Test environment
        con = databaseTestConfig.getConnection();

        // Initialize our DAO with test environment
        ticketDAO = new TicketDAO(databaseTestConfig);

        // Clearing previous tested entries
        dataBasePrepareService = new DataBasePrepareService();
        dataBasePrepareService.clearDataBaseEntries();

        // CREATING TEST ENTRIES IN DATABASE
        Date inTime = new Date(System.currentTimeMillis());
        Date outTime = new Date(System.currentTimeMillis() + (60 * 60 * 1000));

        Timestamp timestampIn = new Timestamp(inTime.getTime());
        Timestamp timestampOut = new Timestamp(outTime.getTime());

        // Query : Save a ticket in database to check if vehicle is regular or not
        PreparedStatement ps = con.prepareStatement(DBConstants.SAVE_TICKET);
        ps.setInt(1, 1);
        ps.setString(2, REGISTERED_PLATE);
        ps.setDouble(3, 1.0);
        ps.setTimestamp(4, timestampIn);
        ps.setTimestamp(5, timestampOut);
        ps.execute();
    }

    @Test
    @DisplayName("Ticket is saved in database and returns VEHICLE_REG_NUMBER")
    void saveTicket_shouldReturnTrue() throws Exception {
        // GIVEN
        Ticket ticket = new Ticket();
        Date inTime = new Date();

        ParkingSpot parkingSpot = new ParkingSpot(2, ParkingType.CAR, true);
        ticket.setParkingSpot(parkingSpot);
        ticket.setVehicleRegNumber(NEW_VEHICLE_PLATE);
        ticket.setPrice(0.0);
        ticket.setInTime(inTime);
        ticket.setOutTime(null);

        // WHEN
        ticketDAO.saveTicket(ticket);
        String actualPlate = ticketDAO.getTicket(NEW_VEHICLE_PLATE).getVehicleRegNumber();

        // THEN
        assertEquals(NEW_VEHICLE_PLATE, actualPlate);
    }

    @Test
    @DisplayName("Ticket is update in database and returns VEHICLE_REG_NUMBER")
    void updateTicket_shouldReturnTrue() throws Exception {
        // GIVEN
        Ticket ticket = new Ticket();
        Date inTime = new Date();

        ParkingSpot parkingSpot = new ParkingSpot(2, ParkingType.CAR, true);
        ticket.setId(2);
        ticket.setParkingSpot(parkingSpot);
        ticket.setVehicleRegNumber(NEW_VEHICLE_PLATE);
        ticket.setPrice(0.0);
        ticket.setInTime(inTime);
        ticket.setOutTime(null);
        ticketDAO.saveTicket(ticket);

        // WHEN
        ticket.setOutTime(DateUtils.addHours(inTime, 24));
        ticket.setPrice(8);
        ticketDAO.updateTicket(ticket);
        Ticket ticketUpdate = ticketDAO.getTicket(NEW_VEHICLE_PLATE);

        // THEN
        assertNotNull(ticketUpdate.getOutTime());
        assertEquals(8, ticketUpdate.getPrice());
    }

    @Test
    void isARecurrentUserTest() throws Exception {
        // WHEN
        Boolean recurrentUser = ticketDAO.isARecurrentUser("QSDFG");

        // THEN
        assertEquals(false, recurrentUser);
    }

    @Test
    void vehicleAlreadyPresentTest() throws Exception {
        // GIVEN
        Ticket ticket = new Ticket();
        Date inTime = new Date();

        ParkingSpot parkingSpot = new ParkingSpot(2, ParkingType.CAR, true);
        ticket.setParkingSpot(parkingSpot);
        ticket.setVehicleRegNumber(NEW_VEHICLE_PLATE);
        ticket.setPrice(0.0);
        ticket.setInTime(inTime);
        ticket.setOutTime(null);
        ticketDAO.saveTicket(ticket);

        // WHEN
        Boolean alreadyPresent = ticketDAO.vehicleAlreadyPresent("NEW");

        // THEN
        assertEquals(true, alreadyPresent);
    }
}
