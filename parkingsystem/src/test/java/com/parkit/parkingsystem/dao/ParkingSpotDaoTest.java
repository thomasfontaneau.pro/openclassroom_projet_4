package com.parkit.parkingsystem.dao;

import com.parkit.parkingsystem.config.DataBaseConfig;
import com.parkit.parkingsystem.constants.ParkingType;
import com.parkit.parkingsystem.integration.config.DataBaseTestConfig;
import com.parkit.parkingsystem.integration.service.DataBasePrepareService;
import com.parkit.parkingsystem.model.ParkingSpot;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParkingSpotDaoTest {

    private static DataBasePrepareService dataBasePrepareService;

    private static DataBaseConfig databaseTestConfig = new DataBaseTestConfig();

    private static ParkingSpotDAO parkingSpotDAO = new ParkingSpotDAO(databaseTestConfig);

    @Test
    void getNextAvailableSlotTest() throws Exception {
        ParkingType parkingType = ParkingType.CAR;

        // WHEN
        int parkingNumber = parkingSpotDAO.getNextAvailableSlot(parkingType);

        // THEN
        assertEquals(1, parkingNumber);
    }

    @Test
    void updateParkingTest() throws Exception {
        // GIVEN
        ParkingType parkingType = ParkingType.CAR;
        ParkingSpot parkingSpot = new ParkingSpot(1, parkingType, false);

        // WHEN
        Boolean updateOk = parkingSpotDAO.updateParking(parkingSpot);

        // THEN
        assertEquals(true, updateOk);
    }
}
