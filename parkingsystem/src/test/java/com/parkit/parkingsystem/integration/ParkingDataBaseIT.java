package com.parkit.parkingsystem.integration;

import com.parkit.parkingsystem.constants.ParkingType;
import com.parkit.parkingsystem.dao.ParkingSpotDAO;
import com.parkit.parkingsystem.dao.TicketDAO;
import com.parkit.parkingsystem.integration.config.DataBaseTestConfig;
import com.parkit.parkingsystem.integration.service.DataBasePrepareService;
import com.parkit.parkingsystem.model.Ticket;
import com.parkit.parkingsystem.service.ParkingService;
import com.parkit.parkingsystem.util.InputReaderUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ParkingDataBaseIT {

    private DataBaseTestConfig dataBaseTestConfig = new DataBaseTestConfig();
    private ParkingSpotDAO parkingSpotDAO;
    private static DataBasePrepareService dataBasePrepareService = new DataBasePrepareService();
    private TicketDAO ticketDAO;
    private ParkingService parkingService;

    @Mock
    private InputReaderUtil inputReaderUtilMock;

    @BeforeAll
    private static void setUp() {
        dataBasePrepareService.clearDataBaseEntries();
    }

    @BeforeEach
    private void setUpPerTest() throws Exception {
        parkingSpotDAO = new ParkingSpotDAO(dataBaseTestConfig);
        ticketDAO = new TicketDAO(dataBaseTestConfig);
        dataBasePrepareService = new DataBasePrepareService();
    }

    @AfterAll
    private static void tearDown(){

    }

    @Test
    public void testParkingACar() throws Exception{
        // GIVEN
        when(inputReaderUtilMock.readSelection()).thenReturn(1);
        when(inputReaderUtilMock.readVehicleRegistrationNumber()).thenReturn("ABCDEF");

        // WHEN
        parkingService = new ParkingService(inputReaderUtilMock, parkingSpotDAO, ticketDAO);
        parkingService.processIncomingVehicle();
        Ticket ticket = ticketDAO.getTicket("ABCDEF");

        // THEN
        Assertions.assertNotNull(ticket);
        Assertions.assertEquals(2,parkingSpotDAO.getNextAvailableSlot(ParkingType.CAR));
    }

    @Test
    public void testParkingLotExit() throws Exception{
        // GIVEN
        when(inputReaderUtilMock.readVehicleRegistrationNumber()).thenReturn("ABCDEF");
        Thread.sleep(1000);
        testParkingACar();

        // WHEN
        ParkingService parkingService = new ParkingService(inputReaderUtilMock, parkingSpotDAO, ticketDAO);
        parkingService.processExitingVehicle();
        Ticket ticket = ticketDAO.getTicket("ABCDEF");

        // THEN
        Assertions.assertNotNull(ticket.getOutTime());
        Assertions.assertEquals(1,parkingSpotDAO.getNextAvailableSlot(ParkingType.CAR));
    }
}
