package com.parkit.parkingsystem;

import com.parkit.parkingsystem.config.DataBaseConfig;
import com.parkit.parkingsystem.constants.ParkingType;
import com.parkit.parkingsystem.dao.ParkingSpotDAO;
import com.parkit.parkingsystem.dao.TicketDAO;
import com.parkit.parkingsystem.model.ParkingSpot;
import com.parkit.parkingsystem.model.Ticket;
import com.parkit.parkingsystem.service.InteractiveShell;
import com.parkit.parkingsystem.service.ParkingService;
import com.parkit.parkingsystem.util.InputReaderUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.sql.Connection;

import java.sql.SQLException;
import java.util.Date;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ParkingServiceTest {

    private static ParkingService parkingService;

    @Mock
    private InputReaderUtil inputReaderUtilMock;
    @Mock
    private ParkingSpotDAO parkingSpotDAO;
    @Mock
    private TicketDAO ticketDAO;

    Ticket ticket;

    @BeforeEach
    private void setUpPerTest() {
        try {
            ParkingSpot parkingSpot = new ParkingSpot(1, ParkingType.CAR,false);
            ticket = new Ticket();
            ticket.setInTime(new Date(System.currentTimeMillis() - (60*60*1000)));
            ticket.setParkingSpot(parkingSpot);
            ticket.setVehicleRegNumber("ABCDEF");

            parkingService = new ParkingService(inputReaderUtilMock, parkingSpotDAO, ticketDAO);
        } catch (Exception e) {
            e.printStackTrace();
            throw  new RuntimeException("Failed to set up test mock objects");
        }
    }

    @Test
    public void processExitingVehicleTest() throws Exception {
        // GIVEN
        when(inputReaderUtilMock.readVehicleRegistrationNumber()).thenReturn("ABCDEF");
        when(ticketDAO.vehicleAlreadyPresent("ABCDEF")).thenReturn(true);
        when(ticketDAO.getTicket(anyString())).thenReturn(ticket);
        when(ticketDAO.updateTicket(any(Ticket.class))).thenReturn(true);
        when(parkingSpotDAO.updateParking(any(ParkingSpot.class))).thenReturn(true);

        // WHEN
        parkingService.processExitingVehicle();

        // THEN
        verify(parkingSpotDAO, Mockito.times(1)).updateParking(any(ParkingSpot.class));
    }

    @Test
    public void processIncomingVehicle() throws Exception {
        // GIVEN
        when(inputReaderUtilMock.readSelection()).thenReturn(1);
        when(inputReaderUtilMock.readVehicleRegistrationNumber()).thenReturn("ABCDEF");
        when(ticketDAO.vehicleAlreadyPresent(anyString())).thenReturn(false);
        when(parkingSpotDAO.updateParking(any(ParkingSpot.class))).thenReturn(true);
        when(parkingSpotDAO.getNextAvailableSlot(ParkingType.CAR)).thenReturn(1);

        // WHEN
        parkingService.processIncomingVehicle();

        // THEN
        verify(parkingSpotDAO, Mockito.times(1)).updateParking(any(ParkingSpot.class));
    }

    @Test
    public void getVehichleTypeTest(){
        // GIVEN
        when(inputReaderUtilMock.readSelection()).thenReturn(1);

        // WHEN
        ParkingType parkingType = parkingService.getVehicleType();

        // THEN
        assertEquals(ParkingType.CAR, parkingType);
    }


    @Test
    public void getNextParkingNumberIfAvailableTest(){
        // GIVEN
        when(inputReaderUtilMock.readSelection()).thenReturn(1);
        when(parkingSpotDAO.getNextAvailableSlot(ParkingType.CAR)).thenReturn(1);

        // WHEN
        ParkingSpot parkingSpot = parkingService.getNextParkingNumberIfAvailable();

        // THEN
        assertEquals(1, parkingSpot.getId());
    }

    @Test
    public void getConnectionTest() throws SQLException, IOException, ClassNotFoundException {
        // GIVEN
        DataBaseConfig dataBaseConfig = new DataBaseConfig();

        //When
        Connection connection = dataBaseConfig.getConnection();

        //THEN
        assertEquals("jdbc:mysql://localhost:3306/prod", connection.getMetaData().getURL());
        assertFalse(connection.isClosed());
    }

    @Test
    public void closeConnectionTest() throws SQLException, IOException, ClassNotFoundException {
        // GIVEN
        DataBaseConfig dataBaseConfig = new DataBaseConfig();
        Connection connection = dataBaseConfig.getConnection();

        //When
        dataBaseConfig.closeConnection(connection);

        //THEN
        assertTrue(connection.isClosed());
    }

}
